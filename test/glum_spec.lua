--Tests glum to make sure it works correctly!

--Come config stuff, you can edit this to your likeing
local config = {
	lscmd = "ls -a \"%s\"", -- List directory contents command
	ignoredirs = {".",".."} -- List of directories that appear in the above command that we should ignore
}

local tests = {}
describe("Finding tests",function()
	--A function that looks for files in a directory, you might need to rewrite this for windows
	local function scan_paths(path,tbl)
		local pfile = io.popen(string.format(config.lscmd,path))
		local pdata = pfile:read("*all")
		for filename in string.gmatch(pdata,"[^\n]+") do
			if filename:find("%.lua$") then --It's a file, include it  on the path
				tbl[path .. "/" .. filename] = true
			else
				local shouldignore = false

				for k,v in pairs(config.ignoredirs) do
					if filename == v then
						shouldignore = true
						break
					end
				end

				if not shouldignore then
					scan_paths(path .. "/" .. filename,tbl)
				end
			end
		end
	end

	local t = {}
	scan_paths("./tests",t)
	tests = t
end)

local glum
describe("Finding GLuM",function()
	glum = dofile("../src/glum.lua")
end)

describe("GLuM", function()
	for k,v in pairs(tests) do
		local testcode = loadstring(v)
		testcode()
	end
end)
