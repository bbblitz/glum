local something = {}

something.givenumber = function(self,num)
    print("Thanks, I love " .. num .. "! It's much better than " .. self.lastnum)
    self.lastnum = num
end

something.lastnum = 0

something:givenumber(1)
something:givenumber(2)
something:givenumber(10)
