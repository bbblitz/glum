describe("Should minify calls to other functions correctly",function()
	local minitxt = glum.
end)
function afunction()
    print("You called a function!")
end

function anotherfunction(num)
    print("Thanks, I love " .. num .. "!")
    return num == 0 and 1 or anotherfunction(num-1)
end

local function yetanotherfunction(num)
    print("Ew, I don't like " .. num .. ".")
    return num <= 0 and 1 or yetanotherfunction(num/2)
end

afunction()

for k = 100,1000,100 do
    anotherfunction(k)
end

yetanotherfunction(2000)
